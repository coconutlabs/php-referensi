//STD
//Agar bisa nginput di Command-line seperti scanf() pada Bahasa C
//Note: Khusus untuk dijalankan di Command-Line/Console/Terminal
//Contoh Menjalankannya:
//root@coco:~/#php inputOutputCLI.php

<?php
echo "Masukkan String: ";
fscanf(STDIN, "%s\n", $x);
echo $x,"\n";
echo "Masukkan Integer/Angka: ";
fscanf(STDIN, "%d\n", $y);
echo $y,"\n";
?>