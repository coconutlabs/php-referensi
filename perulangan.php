<?php
/*
* looping atau perulangan digunakan untuk melakukan proses yang berulang ulang
* anda dapat menambahkan beberapa fungsi atau script kode yang lain di dalam perulangan tersebut sesuai kebutuhan
*
*
* perulangan for
*/
 
 for($no=0;$no<=5;$no++){
 
 echo "ini adalah perulangan ke ".$no;
 }

/* for((...kondisimulai...);(...kondisi berhenti...);(...penambahan/pengurangan...){ 
*       (...kode yang akan di ulang ...)   
*       }
*/

?>