<?php
/////////////////////////
echo "<h2>Contoh 1:</h3><br />"; //contoh 1

$nilai = array( 88, 99, 66,"tes string" ,55, 44);
for ($i=0; $i<=5; $i++)
{
  echo "Variabel nilai yang ke $i berisi $nilai[$i] <br />";
}

/////////////////////////
echo "<h2>Contoh 2:</h3><br />";//contoh 2

$nama[1] = "uke";
$nama[2] = "seno";
$nama[3] = "fira";
$nama[4] = "asis";
echo "output variabel array yang ke empat adalah: $nama[4] <br />";

/////////////////////////
echo "<h2>Contoh 3:</h3><br />";//contoh 3 (yang ini mirip contoh 1, tp non perulangan

$id = array("uke","seno","fira","asis");
echo "nama dalam variabel id[0] adalah: " . $id[0] ."<br />";
echo "nama dalam variabel id[2] adalah: " . $id[2] ."<br />"; 

/////////////////////////
echo "<h2>Contoh 4:</h3><br />";//contoh 4 (yang ini nilai dalam sebuah array)

$anggota['uke'] = "puasa";
$anggota['fira'] = "nda puasa";
$anggota['seno'] = "puasa stengah";
$anggota['asis'] = "lagi haid";

echo $anggota['asis'];

////////////////////////
echo "<h2>Contoh 5:</h3><br />";//contoh 5 (yang ini namanya multi-dimensional array)
//atau istilahnya array didalam array - 
//yang ini butuh nalar. dan kalau mau cpt menger silahkan diplototin lama2 :p
//anggap saja salah satu data dibawah ini ada nama: uke, dgn usia 22, nilai masak 7 dan kelamin wanita
//begitupun dengan data yang lainnya, seperti fira dengan usia 18, nilai masak 6 dan kelamin wanita.

$orang = array
   (
   array("uke",22,7,"wanita"),
   array("fira",18,6,"wanita"),
   array("seno",21,5,"pria"),
   array("asis",19,4,"pria")
   );

echo $orang[0][0].": berusia: ".$orang[0][1].", dangan nilai masak: ".$orang[0][2]." dan kelamin: ".$orang[0][3]."<br />";
echo $orang[2][0] ."<br />"; //baris ini cetak nama seno
echo $orang[2][3] ."<br />"; //baris ini cetak kelamin seno
echo "Coba Tampilkan usia fira disini: "; //baris ini sebagai tempat latihanmu/improvisasimu.

//kesimpulan: menggunakan fungsi array() itu nilai awalnya dimulai dari 0.
//diatas ada 5 fungsi array(), 1 sebagai fungsi array utama, dan 4 sisanya sebagai fungsi didalam fungsi array utama.

?>
